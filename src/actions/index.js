// All Action Creators:

// Needs to be called whenever user clicks on any of the book.
// book is an object with a title

// This needs to be wired up to redux as well.
export function selectBook(book) {
    // selectBook is an action creator, it needs to return an action object.
    // an action object with a type property and a payload(optional) property.
    return {
        type: 'BOOK_SELECTED',
        payload: book
    };
}

