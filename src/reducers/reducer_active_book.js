/**
 * Created by lokeshagrawal on 11/05/17.
 */

// reducer function.
// All reducers gets two arguments, the current state and an action. So reducres are only ever called when an action occurs.

// state argument is not an application state, only the state this reducer is responsible for. With this previous state keeps flowing into this reducer over and over again.

// we will have to handle the case, when users first boots up the app and no book is immediately available. state passed to the reducer will be undefined and reducer will return undefined
// and redux dont allow us to return undefined from reducers, it throws error. That's why default the state to null (ES6 way).
export default function(state = null, action) {
    switch(action.type) {
        case "BOOK_SELECTED":

            // Never mutate the state and return, always return the fresh object.
            // state.title = book.title
            // return state
            return action.payload;
    }
    // base case, if we dont care about the action passed, return last state.
    return state;
}