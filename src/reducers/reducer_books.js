/**
 * Created by lokeshagrawal on 10/05/17.
 */

// Reducer function
export default function() {

    // function returning array of books
    return [
        {title: 'Javascript: The Good Parts', pages: 101},
        {title: 'Harry Potter', pages: 84},
        {title: 'The Dark Tower', pages: 23},
        {title: 'Eloquent Ruby', pages: 11}
    ];
}